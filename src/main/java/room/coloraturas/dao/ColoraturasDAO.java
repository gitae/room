package room.coloraturas.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import room.common.dao.AbstractDAO;

@Repository("coloraturasDAO")
public class ColoraturasDAO extends AbstractDAO{
	
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> selectTest(Map<String, Object> map) throws Exception{
		return (List<Map<String, Object>>)selectList("coloraturas.selectTest", map);
	}

	/*
	public void insertBoard(Map<String, Object> map) throws Exception{
		insert("coloraturas.insertBoard", map);
	}

	public void updateHitCnt(Map<String, Object> map) throws Exception{
		update("coloraturas.updateHitCnt", map);
	}

	@SuppressWarnings("unchecked")
	public Map<String, Object> selectBoardDetail(Map<String, Object> map) throws Exception{
		return (Map<String, Object>) selectOne("coloraturas.selectBoardDetail", map);
	}

	public void updateBoard(Map<String, Object> map) throws Exception{
		update("coloraturas.updateBoard", map);
	}

	public void deleteBoard(Map<String, Object> map) throws Exception{
		update("coloraturas.deleteBoard", map);
	}
	*/
}
