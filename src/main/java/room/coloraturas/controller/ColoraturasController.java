package room.coloraturas.controller;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import room.coloraturas.service.ColoraturasService;
import room.common.common.CommandMap;

@Controller
public class ColoraturasController {
	Logger log = Logger.getLogger(this.getClass());
	
	@Resource(name="coloraturasService")
	private ColoraturasService coloraturasService;
	
	@RequestMapping(value="/coloraturas/main.do")
    public ModelAndView openBoardList(CommandMap commandMap) throws Exception{
    	ModelAndView mv = new ModelAndView("/main");
    	
    	List<Map<String,Object>> list = coloraturasService.selectTest(commandMap.getMap());
    	mv.addObject("list", list);
    	
    	return mv;
    }
	
	@RequestMapping(value="/coloraturas/board.do")
    public ModelAndView openBoard(CommandMap commandMap) throws Exception{
    		ModelAndView mv = new ModelAndView("/board_standard");
    	
    	return mv;
	}
	
}
