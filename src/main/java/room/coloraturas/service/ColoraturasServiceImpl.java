package room.coloraturas.service;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import room.coloraturas.dao.ColoraturasDAO;

@Service("coloraturasService")
public class ColoraturasServiceImpl implements ColoraturasService{
	Logger log = Logger.getLogger(this.getClass());
	
	@Resource(name="coloraturasDAO")
	private ColoraturasDAO coloraturasDAO;
	
	@Override
	public List<Map<String, Object>> selectTest(Map<String, Object> map) throws Exception {
		return coloraturasDAO.selectTest(map);
		
	}
	
	/*
	@Override
	public void insertBoard(Map<String, Object> map) throws Exception {
		coloraturasDAO.insertBoard(map);
	}

	@Override
	public Map<String, Object> selectBoardDetail(Map<String, Object> map) throws Exception {
		coloraturasDAO.updateHitCnt(map);
		Map<String, Object> resultMap = coloraturasDAO.selectBoardDetail(map);
		return resultMap;
	}

	@Override
	public void updateBoard(Map<String, Object> map) throws Exception{
		coloraturasDAO.updateBoard(map);
	}

	@Override
	public void deleteBoard(Map<String, Object> map) throws Exception {
		coloraturasDAO.deleteBoard(map);
	}
	*/

}
