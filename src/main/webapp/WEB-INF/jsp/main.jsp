<%@ page pageEncoding="utf-8"%>

<%@ include file="/WEB-INF/include/include-header.jspf" %>

<!-- Main -->
	<div id="main">
	
							<!-- One -->
								<section id="one" class="tiles">
									<article>
										<span class="image">
											<img src="${contextPath}/static/images/pic01.jpg" alt="" />
										</span>
										<header class="major">
											<h3><a href="landing.html" class="link">Aliquam</a></h3>
											<p>Ipsum dolor sit amet</p>
										</header>
									</article>
									<article>
										<span class="image">
											<img src="${contextPath}/static/images/pic02.jpg" alt="" />
										</span>
										<header class="major">
											<h3><a href="landing.html" class="link">Tempus</a></h3>
											<p>feugiat amet tempus</p>
										</header>
									</article>
									<article>
										<span class="image">
											<img src="${contextPath}/static/images/pic03.jpg" alt="" />
										</span>
										<header class="major">
											<h3><a href="landing.html" class="link">Magna</a></h3>
											<p>Lorem etiam nullam</p>
										</header>
									</article>
									<article>
										<span class="image">
											<img src="${contextPath}/static/images/pic04.jpg" alt="" />
										</span>
										<header class="major">
											<h3><a href="landing.html" class="link">Ipsum</a></h3>
											<p>Nisl sed aliquam</p>
										</header>
									</article>
									<article>
										<span class="image">
											<img src="${contextPath}/static/images/pic05.jpg" alt="" />
										</span>
										<header class="major">
											<h3><a href="landing.html" class="link">Consequat</a></h3>
											<p>Ipsum dolor sit amet</p>
										</header>
									</article>
									<article>
										<span class="image">
											<img src="${contextPath}/static/images/pic06.jpg" alt="" />
										</span>
										<header class="major">
											<h3><a href="landing.html" class="link">Etiam</a></h3>
											<p>Feugiat amet tempus</p>
										</header>
									</article>
								</section>
	
							<!-- Two -->
								<section id="two">
									<div class="inner">
										<header class="major">
											<h2>제안하기</h2>
										</header>
										<p>여러분의 아이디어를 공유해주실 수 있는 공간을 구축하고 있습니다. 
										    새로운 도전을 위한 꿈으로 조금씩 개발해 나가고 있습니다. 다가올 공간을 기대해주세요.</p>
										<ul class="actions">
											<!-- <li><a href="landing.html" class="button next">ooo</a></li> -->
										</ul>
									</div>
								</section>
	
						</div>
	
					<!-- Contact -->
						<section id="contact">
							<div class="inner">
								<section>
									<form method="post" action="#">
										<div class="fields">
											<div class="field half">
												<label for="name">Name</label>
												<input type="text" name="name" id="name" />
											</div>
											<div class="field half">
												<label for="email">Email</label>
												<input type="text" name="email" id="email" />
											</div>
											<div class="field">
												<label for="message">Message</label>
												<textarea name="message" id="message" rows="6"></textarea>
											</div>
										</div>
										<ul class="actions">
											<li><input type="submit" value="Send Message" class="primary" /></li>
											<li><input type="reset" value="Clear" /></li>
										</ul>
									</form>
								</section>
								<section class="split">
									<section>
										<div class="contact-method">
											<span class="icon solid alt fa-envelope"></span>
											<h3>Email</h3>
											<a href="#">kimgitae@icloud.com</a>
										</div>
									</section>
									<section>
										<div class="contact-method">
											<span class="icon solid alt fa-phone"></span>
											<h3>Phone</h3>
											<span>(000) 000-0000</span>
										</div>
									</section>
									<section>
										<div class="contact-method">
											<span class="icon solid alt fa-home"></span>
											<h3>Address</h3>
											<span> Seo-gu, Incheon <br />
											Cheongna hannae-ro, 22758<br />
											Republic of Korea</span>
										</div>
									</section>
								</section>
							</div>
						</section>


<%@ include file="/WEB-INF/include/include-footer.jspf" %>